# OKJIP_auto_analyses
Chlorophyll a fluorescence induction curves analyses with R

This script used some R packages, thanks to their developpers for their good work. I wrote this to help Rnewbies to analyses their data. 
There is no commercial interest, you can use this, but please cite the different packages used.

This script was developped for OKJIP data resulting from chlorophyll a fluorescence measurements with PEA instruments, you can used it with others dataset with some modifications. 

Content:
- export mean and error standard tables by category
- export statistical differences for all parameters by category
- visualization: heatmap based on means, do not reflect statistical differences

See the Wiki page for details

For more information on chlorophyll a fluorescence see the OJIP_help.pdf and to see results of this script, see: https://doi.org/10.3389/fpls.2016.01679

----------------------------------------------------------------
Please cite these packages if you use this code.

__References:__

Gregory R. Warnes, Ben Bolker, Lodewijk Bonebakker, Robert Gentleman, Wolfgang Huber Andy Liaw, Thomas
  Lumley, Martin Maechler, Arni Magnusson, Steffen Moeller, Marc Schwartz and Bill Venables (2016). gplots:
  Various R Programming Tools for Plotting Data. R package version 3.0.1.
  https://CRAN.R-project.org/package=gplots

John Fox and Sanford Weisberg (2011). An {R} Companion to Applied Regression, Second Edition. Thousand
  Oaks CA: Sage. URL: http://socserv.socsci.mcmaster.ca/jfox/Books/Companion
  
Laercio Junio da Silva (2010). laercio: Duncan test, Tukey test and Scott-Knott test.. R package version
  1.0-1. https://CRAN.R-project.org/package=laercio
   
Patrick Giraudoux (2017). pgirmess: Data Analysis in Ecology. R package version 1.6.7.
  https://CRAN.R-project.org/package=pgirmess
  

-----------------------------------------------------------------
__License__

See the [LICENSE](https://gitlab.com/RipollJ/OKJIP_auto_analyses/blob/master/License.md) file for license rights and limitations (MIT).
