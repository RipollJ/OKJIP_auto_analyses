############################
### OKLJIP data analyses ###
############################

# Author: RipollJ
# Last_update: 2018-01-28

# License
# See the LICENSE file for license rights and limitations (MIT).

### import data
data <- read.delim("OKJIP_results_parameters.txt", header=T, sep="\t")
str(data) # see file information
summary(data) # summarize data

# check if NA in data
if (!is.na(data)==FALSE) print("yes")
  else {print("no")}

# Install packages
ilist<-c("laercio","car","gplots","pgirmess")
if (!require(ilist)) install.packages(ilist)

# launch package
library(car)
library(laercio)
library(pgirmess)
library(gplots)

# definition of contrasts
options(contrasts=c("contr.sum","contr.sum")) # by mean

### data information
# define a unique identifier
data$ID <- interaction(data$Genotype, data$Treatment)

# calcul of means for unique identifier
MEAN <- aggregate(data[,-c(1)],by=list(data$ID),mean,na.rm=T)
# alternative MEAN <- aggregate(data[,c(1,2)],by=list(data$Genotype,data$Treatment),mean,na.rm=T)
MEAN <- as.data.frame(MEAN)
MEAN
# export table
write.table(MEAN,"Results_OKJIP_means.txt",dec=".",quote=FALSE,row.names=FALSE)

# function to calculate standard error
SE <- function(data){
  sterr <- sqrt(var(data,na.rm=TRUE)/ length(data[is.na(data)!=TRUE]))
  return(sterr)
}
# calcul
SE <- aggregate(data[,-c(1,2)],by=list(data$Genotype,data$Treatment),SE)
SE <- as.data.frame(SE)
SE
# export table
write.table(SE,"Results_OKJIP_SE.txt",dec=".",quote=FALSE,row.names=FALSE)

### calcul of statistical differences with correction of data (sqrt, scaled/centered, log)
# you can modify type of correction and test used in the loop according to your type of data and need

# export of results
sink(file="Results.txt",append=F)

#loop with conditions, for one ID
#names(data)
i=1
# we assume that ID are in column one
for (i in c(2:lenght(data)) { 
	x<-data[,i]
    print(colnames(DATA))
    print("### Column number = variable checked ###")
    print("Column number = ")
    print(i)
	R1<-aov(x~ID, data=data) # un seul identifiant
	aov1<-anova(R1)
	print(aov1)
	# Check normality of residuals
	res<-residuals(R1)
	shapiro.test(res)
	P<-shapiro.test(res)$p.value
	hist(res,col="grey",main="Histogramme des residus",ylab="Frequence",xlab="valeur des residus")
	# Check homoscedasticity of variances
	Leven<-anova(lm((R1$res^2)~R1$model[,-1]))
	p.value2<-Leven$`Pr(>F)`[1]
	# Comparisons
	if(P>0.05&p.value2>0.05){print(LDuncan(R1))}
		else { R2<-aov(sqrt(abs(x))~ID,data=data)
		aov2<-anova(R2)
		res<-residuals(R2)
		Leven<-anova(lm((R2$res^2)~R2$model[,-1]))
		p.value2<-Leven$`Pr(>F)`[1]}
		if(p.value2>0.05){print("square root")
		print(LDuncan(R2))}
			else { x2<-scale(x,center=T,scale=T)
			R2b<-aov(x2~ID,data=data)
			aov2<-anova(R2b)
			res<-residuals(R2b)
			Leven<-anova(lm((R2b$res^2)~R2b$model[,-1]))
			p.value2b<-Leven$`Pr(>F)`[1]}
			if(p.value2b>0.05){print("scaled and reduced")
			print(LDuncan(R2b))}
				else { R2c<-aov(log(x+1)~ID,data=data)
				aov2<-anova(R2c)
				res<-residuals(R2c)
				Leven<-anova(lm((R2c$res^2)~R2c$model[,-1]))
				p.value2b<-Leven$`Pr(>F)`[1]}
				if(p.value2b>0.05){print("log+1")
				print(LDuncan(R2c))}
					else { KW<-kruskal.test(x~data$ID)
					p.value4<-KW$p.value
					if(p.value4<0.05){
					KW2<-kruskalmc(x~data$ID, data=NULL, probs=0.05)
					KW2b<-KW2$dif.com
					KW3<-KW2b[KW2b$difference==TRUE,]
					print(KW3)}
					else{print("no difference")}
					}
i=i+1
}

sink()

### Visualization of data

# heatmap
DATA.HEATMAP=as.matrix(MEAN)
head(DATA.HEATMAP)
# open a plot window
x11(width=250, height=250)
heatmap.2 (DATA.HEATMAP,
           # dendrogram control
           Rowv=NULL,
           distfun = dist,
           hclustfun = hclust,
           # data scaling and centering auto
           scale = c("column"),
           na.rm=TRUE,
		   #na.color="black", # if you want to change color for NA
           # colors
           labRow=rownames(data$ID),
           col=redgreen(100),
           trace="none",
           density.info="density",
           # color key + density info
           key = TRUE,
           key.title = "Normalized value"
)
